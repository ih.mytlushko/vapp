provider "aws" {
  region = "us-east-2"
}

resource "aws_instance" "rmp" {
  ami                = "ami-064ff912f78e3e561"
  instance_type      = "t2.micro"
  tags = {
    Name = "test-test"
  }
}

// resource "aws_vpc" "vpc" {
//   cidr_block       = "10.0.0.0/16"
//   instance_tenancy = "default"

//   tags = {
//     Name = "main"
//   }
// }

// #Subnets
// resource "aws_subnet" "public-subnet-1" {

//   vpc_id                  = aws_vpc.vpc.id
//   cidr_block              = "10.0.0.0/24"
//   map_public_ip_on_launch = true
//   availability_zone       = "us-east-2a"

//   tags      = {
//     Name    = "Public 1"
//   }
// }


// resource "aws_subnet" "private-subnet-1" {

//   vpc_id                   = aws_vpc.vpc.id
//   cidr_block               = "10.0.2.0/24"
//   map_public_ip_on_launch  = false
//   availability_zone        = "us-east-2b"

//   tags      = {
//     Name    = "Private 1"
//   }
// }

// # Internet Gateway
// resource "aws_internet_gateway" "gw" {
//   vpc_id = aws_vpc.vpc.id

//   tags = {
//     Name = "gw"
//   }
// }

// # Route Table
// resource "aws_default_route_table" "route_table" {
//   default_route_table_id = aws_vpc.vpc.default_route_table_id
// route {
//     cidr_block = "0.0.0.0/0"
//     gateway_id = aws_internet_gateway.gw.id
//   }
// tags = {
//     Name = "default route table"
//   }
// }


// # Security Group
// resource "aws_security_group" "js" {
//   name        = "js"
//   description = "Allow SSH and HTTP inbound traffic"
//   vpc_id      = aws_vpc.vpc.id
//   ingress {
//     description = "SSH traffic"
//     protocol    = "tcp"
//     self        = true
//     from_port   = 22
//     to_port     = 22
//     cidr_blocks = ["0.0.0.0/0"]
//   }

//   ingress {  
//     description = "HTTP traffic"
//     protocol    = "tcp"
//     self        = true
//     from_port   = 80
//     to_port     = 80
//     cidr_blocks = ["0.0.0.0/0"]
//   }
//   ingress {  
//     description = "Ping"
//     protocol    = "icmp"
//     self        = true
//     from_port   = -1
//     to_port     = -1
//     cidr_blocks = ["0.0.0.0/0"]
//   }
//   egress {
//     from_port   = 0
//     to_port     = 0
//     protocol    = "-1"
//     cidr_blocks = ["0.0.0.0/0"]
//   }

//   tags = {
//     Name = "SG for js"
//   }

// }

// # Launch Config
// resource "aws_launch_configuration" "js" {
//   name_prefix = "js-"

//   image_id = "ami-056b1936002ca8ede" 
//   instance_type = "t2.micro"
//   key_name = "stkey"

//   security_groups = [ aws_security_group.js.id ]
//   associate_public_ip_address = true
//    user_data = <<EOF
//         #!/bin/bash
//         yum update -y
//         yum install -y docker
//         service docker start
//         docker pull bkimminich/juice-shop
//         docker run -d -p 80:3000 bkimminich/juice-shop
//         EOF
//   lifecycle {
//     create_before_destroy = true
//   }
// }

// resource "aws_security_group" "elb_http" {
//   name        = "elb_http"
//   description = "Allow HTTP traffic to instances through Elastic Load Balancer"
//   vpc_id = aws_vpc.vpc.id

//   ingress {
//     from_port   = 80
//     to_port     = 80
//     protocol    = "tcp"
//     cidr_blocks = ["0.0.0.0/0"]
//   }

//   egress {
//     from_port       = 0
//     to_port         = 0
//     protocol        = "-1"
//     cidr_blocks     = ["0.0.0.0/0"]
//   }

//   tags = {
//     Name = "Allow HTTP through ELB Security Group"
//   }
// }

// resource "aws_elb" "js_elb" {
//   name = "js-elb"
//   security_groups = [
//     aws_security_group.elb_http.id
//   ]
//   subnets = [
//     aws_subnet.public-subnet-1.id,
//     aws_subnet.private-subnet-1.id
//   ]

//   cross_zone_load_balancing   = true

//   health_check {
//     healthy_threshold = 2
//     unhealthy_threshold = 2
//     timeout = 3
//     interval = 30
//     target = "HTTP:80/"
//   }

//   listener {
//     lb_port = 80
//     lb_protocol = "http"
//     instance_port = "80"
//     instance_protocol = "http"
//   }

// }

// resource "aws_autoscaling_group" "js" {
//   name = "${aws_launch_configuration.js.name}-asg"

//   min_size             = 1
//   desired_capacity     = 2
//   max_size             = 4
  
//   health_check_type    = "ELB"
//   load_balancers = [
//     aws_elb.js_elb.id
//   ]

//   launch_configuration = aws_launch_configuration.js.name

//   enabled_metrics = [
//     "GroupMinSize",
//     "GroupMaxSize",
//     "GroupDesiredCapacity",
//     "GroupInServiceInstances",
//     "GroupTotalInstances"
//   ]

//   metrics_granularity = "1Minute"

//   vpc_zone_identifier  = [
//     aws_subnet.public-subnet-1.id,
//     aws_subnet.private-subnet-1.id
//   ]


//   lifecycle {
//     create_before_destroy = true
//   }

//   tag {
//     key                 = "Name"
//     value               = "js"
//     propagate_at_launch = true
//   }

// }




// resource "aws_autoscaling_policy" "js_policy_up" {
//   name = "js_policy_up"
//   scaling_adjustment = 1
//   adjustment_type = "ChangeInCapacity"
//   cooldown = 45
//   autoscaling_group_name = aws_autoscaling_group.js.name
// }

// resource "aws_cloudwatch_metric_alarm" "js_cpu_alarm_up" {
//   alarm_name = "js_cpu_alarm_up"
//   comparison_operator = "GreaterThanOrEqualToThreshold"
//   evaluation_periods = "2"
//   metric_name = "CPUUtilization"
//   namespace = "AWS/EC2"
//   period = "60"
//   statistic = "Average"
//   threshold = "20"

//   dimensions = {
//     AutoScalingGroupName = aws_autoscaling_group.js.name
//   }

//   alarm_description = "This metric monitor EC2 instance CPU utilization"
//   alarm_actions = [ aws_autoscaling_policy.js_policy_up.arn ]
// }


// resource "aws_autoscaling_policy" "js_policy_down" {
//   name = "js_policy_down"
//   scaling_adjustment = -1
//   adjustment_type = "ChangeInCapacity"
//   cooldown = 45
//   autoscaling_group_name = aws_autoscaling_group.js.name
// }

// resource "aws_cloudwatch_metric_alarm" "js_cpu_alarm_down" {
//   alarm_name = "js_cpu_alarm_down"
//   comparison_operator = "LessThanOrEqualToThreshold"
//   evaluation_periods = "2"
//   metric_name = "CPUUtilization"
//   namespace = "AWS/EC2"
//   period = "60"
//   statistic = "Average"
//   threshold = "10"

//   dimensions = {
//     AutoScalingGroupName = aws_autoscaling_group.js.name
//   }

//   alarm_description = "This metric monitor EC2 instance CPU utilization"
//   alarm_actions = [ aws_autoscaling_policy.js_policy_down.arn ]
// }
